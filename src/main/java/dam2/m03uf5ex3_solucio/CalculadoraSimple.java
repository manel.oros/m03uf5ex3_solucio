package dam2.m03uf5ex3_solucio;

import java.util.Stack;

/**
 * Algoritme:
 * 
 * - Mentre existeixin caràcters per llegir:
 *      - Llegir el següent caràcter. Si és:
 *          - nombre: afegir la xifra (ull, pot ser de més d'un caràcter) a la pila de valors (pVal)
 *          - operador: 
 *              - Mentre la pila d'operadors no estigui buida
 *                  - Treure operador de la pila d'operacions (pOper)
 *                  - Treure dos valors de la pila de valors (pVal) en l'ordre correcte
 *                  - Realitzar el càlcul
 *                  - Afegir el resultat a la pila de valors (pVal)
 *              - Posar l'operador a la pila d'operacions (pOper)
 * 
 *  - Mentre existeixin operadors a la pila
 *      - Treure l'operador
 *      - Treure dos valors
 *      - Operar i posar resultat a la pila de valors
 * 
 *  - El valor final a la pila de valors és el resultat de l'operació
 *
 * @author manel
 */
public class CalculadoraSimple {
    
    /***
     * 
     * @param expressio Expressió matemàtca formada per nombres enters i operadors + i -
     * @return 
     */
    public static Integer avalua(String expressio)
	{
            // eliminem espais anteriors i posteriors
            expressio = expressio.trim();

            //array de caracters a tractar
            char[] tokens = expressio.toCharArray();

            // Pila per a valors
            Stack<Integer> values = new Stack<>();

            // Pila per a operadors
            Stack<Character> ops = new Stack<>();

            // recorrem l'expressió un a un, començant per l'esquerra
            for (int i = 0; i < tokens.length; i++)
            {	
                // Si és un espai, l'ignorem i avancem una posició
                if (tokens[i] == ' ') i++;

                // Si és un nombre, llavors el tractem
                if (esNombre(tokens[i]))
                {
                        // preparem un string per a emmagatzemar un nombre de n dígits
                        StringBuilder sbuf = new StringBuilder();

                        // Construcció del nombre complet, que pot estar format per més d'un dígit
                        while (i < tokens.length && esNombre((tokens[i]))) 
                            sbuf.append(tokens[i++]);

                        values.push(Integer.parseInt(sbuf.toString()));

                        // En aquest punt el punter està desplaçat un caràcter més a la dreta del que toca. El fem retrocedir.
                        i--;
                }

                // si és operador admès + -
                else if (esOperador((tokens[i])))
                {
                        // mentre tinguem operadors
                        while (!ops.empty())
                            //els processem
                            values.push(opera(ops.pop(), values.pop(), values.pop()));

                        // en aquest punt ja podem inserir el nou operador
                        ops.push(tokens[i]);
                }
                else
                    throw new IllegalArgumentException(Character.toString(tokens[i]));
            }

            // en aquest punt hem recorregut i simplificat tota l'expressió sencera, sense parèntesis, per tant, operem 
            while (!ops.empty())
                    values.push(opera(ops.pop(), values.pop(), values.pop()));
            
            return values.pop();
	}
    
    /***
     * True si és un nombre de 0 a 9
     * @param s
     * @return 
     */
    private static boolean esNombre(char s)
    {
        boolean ret = false;
        
        if (s >= '0' && s <= '9')
            ret = true;
        
        return ret;
    }
    
    /***
     * True si és un dels següents operadors: + -
     * @param s
     * @return 
     */
    private static boolean esOperador(char s)
    {
        boolean ret = false;
        
        if (s == '+' || s == '-')
            ret = true;
        
        return ret;
    }
    
    /***
     * Suma o resta dos operands
     * @param op
     * @param b
     * @param a
     * @return 
     */
    private static Integer opera(char op,Integer b, Integer a)
	{
            Integer ret = 0;
            
		switch (op)
		{
                    case '+': ret = a + b; break;
                    case '-': ret =  a - b; break;
                }
                
		return ret;
	}
    
}
