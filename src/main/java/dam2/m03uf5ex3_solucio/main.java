package dam2.m03uf5ex3_solucio;

/**
 *
 * @author manel
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try
        {
            System.out.println(CalculadoraSimple.avalua("233-1+6+19-11"));
        }    
        catch (IllegalArgumentException ex)
        {
            System.err.println("El caràcter d'entrada no és numèric ni operador admès: " + ex.getMessage());
        }
        catch (Exception ex)
        {
            System.err.println("S'ha produït un error inesperat al realitzar el càlcul: " + ex);
        }
        
    }
    
}
